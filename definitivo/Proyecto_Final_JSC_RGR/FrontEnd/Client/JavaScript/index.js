//Función "load"
let userFunds;
function start() {
  getCars();
}

//Obtener todos los coches
async function getCars() {
  try {
    const url = "https://localhost:44368/get/cars";

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    console.log(result);
    const table = document.getElementById("tableCars");
    const idUser = parseInt(localStorage.getItem("id"));
    const username = localStorage.getItem("username");
    let tableContent = document.getElementById("tableCars");

    if (username !== null) {
      const cars = result
        .filter((car) => typeof car.brand !== "undefined")
        .map((car) => {
          return {
            id: car.id,
            brand: car.brand,
            model: car.model,
            yearProduced: car.yearProduced,
            type: car.type,
            mileage: car.mileage,
            doors: car.doors,
            carPlate: car.carPlate,
            colour: car.colour,
            price: car.price,
            idOwner: car.idOwner,
            boton:
              username !== null && car.idOwner !== idUser
                ? `<button onclick="buyCar('${car.id}');" class="buyCar"><img class="icon" src="./Assets/comprar.png"></img></button>`
                : "",
          };
        });

      tableContent = cars.reduce(
        (acc, car) => {
          const rol = localStorage.getItem("rol");

          return `${acc} <tr>
              <td>${car.id}</td>
              <td>${car.brand}</td>
              <td>${car.model}</td>
              <td>${car.yearProduced}</td>
              <td>${car.type}</td>
              <td>${car.mileage + " km"}</td>
              <td>${car.doors}</td>
              <td>${car.carPlate}</td>
              <td>${car.colour}</td>
              <td>${car.price + " €"}</td>
              ${
                username !== null && car.idOwner !== idUser
                  ? `<td>${car.boton}</td>`
                  : `<td>Tu vehículo</td>`
              }
          </tr>`;
        },
        ` <tr>
              <th>ID</th>
              <th>Marca</th>
              <th>Modelo</th>
              <th>Año</th>
              <th>Tipo</th>
              <th>Kilometraje</th>
              <th>Puertas</th>
              <th>Matrícula</th>
              <th>Color</th>
              <th>Precio</th>
              <th>Comprar</th>
          </tr>`
      );
    } else {
      const cars = result
        .filter((car) => typeof car.brand !== "undefined")
        .map((car) => {
          return {
            id: car.id,
            brand: car.brand,
            model: car.model,
            yearProduced: car.yearProduced,
            type: car.type,
            mileage: car.mileage,
            doors: car.doors,
            carPlate: car.carPlate,
            colour: car.colour,
            price: car.price,
            idOwner: car.idOwner,
          };
        });

      tableContent = cars.reduce(
        (acc, car) => {
          const rol = localStorage.getItem("rol");

          return `${acc} <tr>
            <td>${car.id}</td>
            <td>${car.brand}</td>
            <td>${car.model}</td>
            <td>${car.yearProduced}</td>
            <td>${car.type}</td>
            <td>${car.mileage + " km"}</td>
            <td>${car.doors}</td>
            <td>${car.carPlate}</td>
            <td>${car.colour}</td>
            <td>${car.price + " €"}</td>
        </tr>`;
        },
        ` <tr>
            <th>ID</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Año</th>
            <th>Tipo</th>
            <th>Kilometraje</th>
            <th>Puertas</th>
            <th>Matrícula</th>
            <th>Color</th>
            <th>Precio</th>
        </tr>`
      );
    }

    table.innerHTML = tableContent;
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    console.log(e);
  }
}

//Obtener un coche específico
async function getCar(carID) {
  try {
    const url = "https://localhost:44368/get/car/" + carID;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    console.log(result);
    return result.price;
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    console.log(e);
    return null;
  }
}

//Obtener todos los datos del usuario
async function getUserData() {
  try {
    const url = "https://localhost:44368/get/user/" + localStorage.id;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    userFunds = result.funds;
    logedIn();
  } catch (e) {
    console.log(e);
  }
}

async function buyCar(carID) {
  try {
    const carPrice = await getCar(carID);
    const message = document.getElementById("carBuyMessage");
    if (carPrice > userFunds) {
      message.innerHTML = "Lo siento, no se ha podido realizar la compra";
    } else {
      const url = "https://localhost:44368/post/buyCar/" + carID;

      const userID = localStorage.getItem("id");

      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: userID,
      });
      const result = await response.json();

      getCars();
      getUserData();
      logedIn();
      message.innerHTML = "Coche comprado con éxito";
    }
  } catch (e) {
    console.log(e);
  }
}

async function logedIn() {
  const textLogIn = document.getElementById("userLogedIn");
  const buttonLogIn = document.getElementById("logInButton");
  const userImage = document.getElementById("userImage");
  const textFunds = document.getElementById("funds");
  textLogIn.style.visibility = "hidden";
  textFunds.style.visibility = "hidden";
  userImage.style.visibility = "hidden";
  if (localStorage.username != null) {
    textFunds.style.visibility = "visible";
    textLogIn.style.visibility = "visible";
    userImage.style.visibility = "visible";
    textLogIn.innerHTML = localStorage.surname + ", " + localStorage.name;
    buttonLogIn.innerHTML = "Log Out";
    textFunds.innerHTML = "Fondos disponibles: " + userFunds + "€";
  } else {
    textLogIn.style.visibility = "hidden";
    buttonLogIn.innerHTML = "Log In";
    userImage.style.visibility = "hidden";
    textFunds.style.visibility = "hidden";
  }
}

async function redirectLogin() {
  const buttonLogIn = document.getElementById("logInButton");
  if (buttonLogIn.innerHTML === "Log In") {
    window.location.href = "login.html";
  } else {
    if (localStorage.username != null) {
      const text = document.getElementById("loginResponse");
      const textLogIn = document.getElementById("logedIn");
      localStorage.removeItem("name");
      localStorage.removeItem("surname");
      localStorage.removeItem("username");
      localStorage.removeItem("role");
      localStorage.removeItem("id");
      window.location.href = "login.html";
    }
  }
  const textLogIn = document.getElementById("logedIn");
}

window.addEventListener("load", start, false);
window.addEventListener("load", logedIn, false);
window.addEventListener("load", getUserData, false);
