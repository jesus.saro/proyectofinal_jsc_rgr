let tableUsers;
let tableCars;
let tableBoughtCars;

//Función "load"
function start() {
  tableCars = document.getElementById("tableCars");
  tableCars.style.visibility = "hidden";
  tableBoughtCars = document.getElementById("tableBoughtCars");
  tableBoughtCars.style.visibility = "hidden";
  tableUsers = document.getElementById("tableUsers");
  tableUsers.style.visibility = "hidden";
  logedIn();
  const funds = document.getElementById("addFunds");
  funds.addEventListener("click", addFunds, false);
}

//Obtener todos los coches en venta de un usuario en específico
async function getInSaleCarsSpecific() {
  try {
    const url = "https://localhost:44368/get/userCars/" + localStorage.id;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    console.log(result);
    const table = document.getElementById("tableCars");

    const cars = result
      .filter((car) => typeof car.brand !== "undefined")
      .map((car) => {
        const user = {
          id: car.id,
          brand: car.brand,
          model: car.model,
          yearProduced: car.yearProduced,
          type: car.type,
          mileage: car.mileage,
          doors: car.doors,
          carPlate: car.carPlate,
          colour: car.colour,
          price: car.price,
          userID: localStorage.getItem("id"),
        };

        return {
          id: car.id,
          brand: car.brand,
          model: car.model,
          yearProduced: car.yearProduced,
          type: car.type,
          mileage: car.mileage,
          doors: car.doors,
          carPlate: car.carPlate,
          colour: car.colour,
          price: car.price,
          botonEdit: `<button onclick="editCar('${car.id}');" class="editCar" value=" X "><img class="icon" src="./Assets/editar.png"></img></button>`,
          botonDelete:
            localStorage.getItem("username") !== null
              ? `<button onclick="deleteInSaleCar('${car.id}');" class="deleteInSaleCar"><img class="icon" src="./Assets/eliminar.png"></img></button>`
              : "",
        };
      });

    const tableContent = cars.reduce(
      (acc, car) => {
        return `${acc} <tr>
              <td>${car.id}</td>
              <td>${car.brand}</td>
              <td>${car.model}</td>
              <td>${car.yearProduced}</td>
              <td>${car.type}</td>
              <td>${car.mileage + " km"}</td>
              <td>${car.doors}</td>
              <td>${car.carPlate}</td>
              <td>${car.colour}</td>
              <td>${car.price + " €"}</td>
              <td>${car.botonEdit}</td>
              ${
                localStorage.getItem("username") !== null
                  ? `<td>${car.botonDelete}</td>`
                  : ""
              }
          </tr>`;
      },
      ` <tr>
              <th>ID</th>
              <th>Marca</th>
              <th>Modelo</th>
              <th>Año</th>
              <th>Tipo</th>
              <th>Kilometraje</th>
              <th>Puertas</th>
              <th>Matrícula</th>
              <th>Color</th>
              <th>Precio</th>
              <th>Editar</th>
              <th>Eliminar</th>
          </tr>`
    );
    table.innerHTML = tableContent;
    table.style.visibility = "visible";

    if (localStorage.role == "Administrador") {
      //const tableUsers = document.getElementById("tableUsers");
      //tableUsers.style.visibility = "visible";
      //getUsers();
    }
  } catch (e) {
    console.log(e);
  }
}

//Obtener todos los coches comprados de un usuario en específico
async function getBoughtCars() {
  try {
    const url = "https://localhost:44368/get/boughtCars/" + localStorage.id;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    console.log(result);
    const table = document.getElementById("tableBoughtCars");

    const cars = result
      .filter((car) => typeof car.brand !== "undefined")
      .map((car) => {
        const user = {
          id: car.id,
          brand: car.brand,
          model: car.model,
          yearProduced: car.yearProduced,
          type: car.type,
          mileage: car.mileage,
          doors: car.doors,
          carPlate: car.carPlate,
          colour: car.colour,
          price: car.price,
          userID: localStorage.getItem("id"),
        };

        return {
          id: car.id,
          brand: car.brand,
          model: car.model,
          yearProduced: car.yearProduced,
          type: car.type,
          mileage: car.mileage,
          doors: car.doors,
          carPlate: car.carPlate,
          colour: car.colour,
          price: car.price,
          botonEdit: `<button onclick="editBoughtCar('${car.id}');" class="editBoughtCar" value=" X "><img class="icon" src="./Assets/editar.png"></img></button>`,
          botonSell: `<button onclick="sellCar('${car.id}');" class="sellCar" value=" X "><img class="icon" src="./Assets/comprar.png"></img></button>`,
          botonDelete:
            localStorage.getItem("username") !== null
              ? `<button onclick="deleteBoughtCar('${car.id}');" class="deleteBoughtCar"><img class="icon" src="./Assets/eliminar.png"></img></button>`
              : "",
        };
      });

    const tableContent = cars.reduce(
      (acc, car) => {
        return `${acc} <tr>
              <td>${car.id}</td>
              <td>${car.brand}</td>
              <td>${car.model}</td>
              <td>${car.yearProduced}</td>
              <td>${car.type}</td>
              <td>${car.mileage + " km"}</td>
              <td>${car.doors}</td>
              <td>${car.carPlate}</td>
              <td>${car.colour}</td>
              <td>${car.price + " €"}</td>
              <td>${car.botonEdit}</td>
              <td>${car.botonSell}</td>
              ${
                localStorage.getItem("username") !== null
                  ? `<td>${car.botonDelete}</td>`
                  : ""
              }
          </tr>`;
      },
      ` <tr>
              <th>ID</th>
              <th>Marca</th>
              <th>Modelo</th>
              <th>Año</th>
              <th>Tipo</th>
              <th>Kilometraje</th>
              <th>Puertas</th>
              <th>Matrícula</th>
              <th>Color</th>
              <th>Precio</th>
              <th>Editar</th>
              <th>Vender</th>
              <th>Eliminar</th>
          </tr>`
    );
    table.innerHTML = tableContent;
    table.style.visibility = "visible";

    if (localStorage.role == "Administrador") {
      //const tableUsers = document.getElementById("tableUsers");
      //tableUsers.style.visibility = "visible";
      //getUsers();
    }
  } catch (e) {
    console.log(e);
  }
}

//Obtener todos los datos del usuario
async function getUserData() {
  try {
    const url = "https://localhost:44368/get/user/" + localStorage.id;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    const name = document.getElementById("name");
    name.innerHTML = result.name;
    const surname = document.getElementById("surname");
    surname.innerHTML = result.surname;
    const username = document.getElementById("username");
    username.innerHTML = result.username;
    const email = document.getElementById("email");
    email.innerHTML = result.email;
    const birthDate = document.getElementById("birthDate");
    birthDate.innerHTML = result.birthDate;
    const funds = document.getElementById("funds");
    funds.innerHTML = result.funds + " €";
  } catch (e) {
    console.log(e);
  }
}

//Obtener todos los usuarios
async function getUsers() {
  try {
    const role = localStorage.getItem("role");
    const string = localStorage.getItem("username");
    if (role === "Desarrollador" || role === "Administrador") {
      const hideText3 = document.getElementById("data3");
      hideText3.style.visibility = "visible";
      tableUsers.style.visibility = "hidden";

      const url = "https://localhost:44368/get/users";

      const response = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const result = await response.json();
      const table = document.getElementById("tableUsers");

      const usuarios = result.map((usuario) => {
        return {
          id: usuario.id,
          name: usuario.name,
          surname: usuario.surname,
          username: usuario.username,
          role: usuario.role,
          email: usuario.email,
          birthDate: usuario.birthDate,
          funds: usuario.funds,
          botonEdit:
            role === "Administrador" || role === "Desarrolador"
              ? `<button onclick="editUser('${usuario.id}');" class="editUser" value=" X "><img class="icon" src="./Assets/editar.png"></img></button>`
              : "",
          botonDelete:
            (role === "Administrador" || role === "Arquitecto") &&
            localStorage.getItem("username") !== usuario.username &&
            usuario.role !== "Administrador"
              ? `<button onclick="deleteUser('${usuario.id}');" class="deleteUser" value=" X "><img class="icon" src="./Assets/eliminar.png"></img></button>`
              : "",
        };
      });

      const tableContent = usuarios.reduce(
        (acc, usuario) => {
          const role = localStorage.getItem("role");

          return `${acc} <tr>
          <td>${usuario.id}</td>
          <td>${usuario.name}</td>
          <td>${usuario.surname}</td>
          <td>${usuario.username}</td>
          <td>${usuario.role}</td>
          <td>${usuario.email}</td>
          <td>${usuario.birthDate}</td>
          <td>${usuario.funds + " €"}</td>
          ${
            role === "Administrador" || role === "Desarrollador"
              ? `<td>${usuario.botonEdit}</td>`
              : ""
          }
          ${
            role === "Administrador" || role === "Desarrollador"
              ? `<td>${usuario.botonDelete}</td>`
              : ""
          }
          </tr>`;
        },
        ` <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Nombre de usuario</th>
            <th>Rol</th>
            <th>Email</th>
            <th>Fecha de nacimiento</th>
            <th>Fondos</th>
            ${
              role === "Administrador" || role === "Desarrollador"
                ? "<th>Editar</th>"
                : ""
            }
              ${
                role === "Administrador" || role === "Desarrollador"
                  ? "<th>Eliminar</th>"
                  : ""
              }
        </tr>`
      );

      table.innerHTML = tableContent;
      table.style.visibility = "visible";
    } else {
      const hideText3 = document.getElementById("data3");
      hideText3.style.visibility = "hidden";
      tableUsers.style.visibility = "hidden";
    }
  } catch (e) {
    console.log(e);
  }
}

//Función para editar un coche
async function editCar(carID) {
  try {
    localStorage.setItem("oldCarID", carID);
    location.href = "editCar.html";
  } catch (e) {
    console.log(e);
  }
}
//Función para editar un coche comprado
async function editBoughtCar(carID) {
  try {
    localStorage.setItem("oldBoughtCarID", carID);
    location.href = "editBoughtCar.html";
  } catch (e) {
    console.log(e);
  }
}

//Función para editar un usuario
async function editUser(userID) {
  try {
    localStorage.setItem("oldUserID", userID);
    location.href = "editUser.html";
  } catch (e) {
    console.log(e);
  }
}

//Función para añadir o retirar fondos
async function addFunds() {
  let quantity = document.getElementById("quantityFunds").value;
  quantity = Math.abs(quantity);
  const sign = document.getElementById("op").value;
  const url = "https://localhost:44368/post/addFunds/" + localStorage.id;
  if (sign === "Añadir") {
    quantity = quantity;
  } else {
    quantity = -1 * quantity;
  }
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: quantity,
    });
    const result = await response.json();
    getUserData();
    getUsers();
  } catch (e) {
    console.log(e);
  }
}

//Función para poner en venta un coche comprado
async function sellCar(carID) {
  try {
    const url = "https://localhost:44368/post/sellBoughtCar/" + carID;

    const userID = localStorage.getItem("id");

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: userID,
    });
    const result = await response.json();
    getInSaleCarsSpecific();
    getBoughtCars();
  } catch (e) {
    console.log(e);
  }
}

//Función para eliminar un coche en venta del usuario
async function deleteInSaleCar(carID) {
  try {
    const url = "https://localhost:44368/delete/inSaleCar/" + carID;

    const userID = localStorage.getItem("id");

    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: userID,
    });
    const result = await response.json();
    getInSaleCarsSpecific();
  } catch (e) {
    console.log(e);
  }
}

//Función para eliminar un coche comprado del usuario
async function deleteBoughtCar(carID) {
  try {
    const url = "https://localhost:44368/delete/boughtCar/" + carID;

    const userID = localStorage.getItem("id");

    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: userID,
    });
    const result = await response.json();
    getBoughtCars();
  } catch (e) {
    console.log(e);
  }
}

//Función para eliminar un usuario
async function deleteUser(userID) {
  try {
    const url = "https://localhost:44368/delete/user/" + userID;

    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();
    getInSaleCarsSpecific();
    getUsers();
  } catch (e) {
    console.log(e);
  }
}

async function logedIn() {
  const textLogIn = document.getElementById("userLogedIn");
  const buttonLogIn = document.getElementById("logInButton");
  const userImage = document.getElementById("userImage");
  const text = document.getElementById("message");
  textLogIn.style.visibility = "hidden";
  userImage.style.visibility = "hidden";
  if (localStorage.username != null) {
    textLogIn.style.visibility = "visible";
    userImage.style.visibility = "visible";
    textLogIn.innerHTML = localStorage.surname + ", " + localStorage.name;
    buttonLogIn.innerHTML = "Log Out";
    text.innerHTML = "Tus datos:";
    const hideText = document.getElementById("data");
    hideText.style.visibility = "visible";
    const hideText2 = document.getElementById("data2");
    hideText2.style.visibility = "visible";
    const hideText3 = document.getElementById("data3");
    hideText3.style.visibility = "visible";
    const hideText4 = document.getElementById("data4");
    hideText4.style.visibility = "visible";
    const hideText5 = document.getElementById("data5");
    hideText5.style.visibility = "visible";
    getInSaleCarsSpecific();
    getBoughtCars();
    getUserData();
    getUsers();
  } else {
    textLogIn.style.visibility = "hidden";
    buttonLogIn.innerHTML = "Log In";
    userImage.style.visibility = "hidden";
    text.innerHTML =
      "Para ver la información de tu perfil debes logearte antes";
    text.style.visibility = "visible";
    const hideText = document.getElementById("data");
    hideText.style.visibility = "hidden";
    const hideText2 = document.getElementById("data2");
    hideText2.style.visibility = "hidden";
    const hideText3 = document.getElementById("data3");
    hideText3.style.visibility = "hidden";
    const hideText4 = document.getElementById("data4");
    hideText4.style.visibility = "hidden";
    const hideText5 = document.getElementById("data5");
    hideText5.style.visibility = "hidden";
  }
}

async function redirectLogin() {
  const buttonLogIn = document.getElementById("logInButton");
  if (buttonLogIn.innerHTML === "Log In") {
    window.location.href = "login.html";
  } else {
    if (localStorage.username != null) {
      const text = document.getElementById("loginResponse");
      const textLogIn = document.getElementById("logedIn");
      localStorage.removeItem("name");
      localStorage.removeItem("surname");
      localStorage.removeItem("username");
      localStorage.removeItem("role");
      localStorage.removeItem("id");
      window.location.href = "login.html";
    }
  }
  const textLogIn = document.getElementById("logedIn");
}

window.addEventListener("load", start, false);
window.addEventListener("load", logedIn, false);
