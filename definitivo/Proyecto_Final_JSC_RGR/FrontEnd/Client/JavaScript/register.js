//Función "load"
function start() {
  const buttonRegister = document.getElementById("register");
  buttonRegister.addEventListener("click", register, false);
}

//Función para obtener los datos del front al registrar un usuario nuevo
async function register() {
  try {
    const text = document.getElementById("resgisterResponse");
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const role = document.getElementById("role").value;
    const birthDate = document.getElementById("birthDate").value;
    const email = document.getElementById("email").value;

    const url = "https://localhost:44368/post/user";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
      body: JSON.stringify({
        name: name,
        surname: surname,
        username: username,
        password: password,
        role: role,
        birthDate: birthDate,
        email: email,
      }),
    });
    const result = await response.json();
    if (result != null) {
      text.innerHTML = "Te has registrado correctamente";
    } else {
      text.innerHTML =
        "No se ha podido crear tu usuario, comprueba que has introducido bien el usuario o la contraseña";
    }
  } catch (e) {
    console.log(e);
  }
}

async function logedIn() {
  const textLogIn = document.getElementById("userLogedIn");
  const buttonLogIn = document.getElementById("logInButton");
  const userImage = document.getElementById("userImage");
  textLogIn.style.visibility = "hidden";
  userImage.style.visibility = "hidden";
  if (localStorage.username != null) {
    textLogIn.style.visibility = "visible";
    userImage.style.visibility = "visible";
    textLogIn.innerHTML = localStorage.surname + ", " + localStorage.name;
    buttonLogIn.innerHTML = "Log Out";
  } else {
    textLogIn.style.visibility = "hidden";
    buttonLogIn.innerHTML = "Log In";
    userImage.style.visibility = "hidden";
  }
}

async function redirectLogin() {
  const buttonLogIn = document.getElementById("logInButton");
  if (buttonLogIn.innerHTML === "Log In") {
    window.location.href = "login.html";
  } else {
    if (localStorage.username != null) {
      const text = document.getElementById("loginResponse");
      const textLogIn = document.getElementById("logedIn");
      localStorage.removeItem("name");
      localStorage.removeItem("surname");
      localStorage.removeItem("username");
      localStorage.removeItem("role");
      localStorage.removeItem("id");
      window.location.href = "login.html";
    }
  }
  const textLogIn = document.getElementById("logedIn");
}

window.addEventListener("load", start, false);
window.addEventListener("load", logedIn, false);
