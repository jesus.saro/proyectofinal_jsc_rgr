//Función "load"
function start() {
  setUserData();
  const buttonSend = document.getElementById("send");
  buttonSend.addEventListener("click", editUser, false);
}

//Obtener los datos del usuario que va a ser modificado
async function setUserData() {
  const text = document.getElementById("result");
  try {
    const url =
      "https://localhost:44368/get/user/" + localStorage.getItem("oldUserID");

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    if (result !== null) {
      document.getElementById("name").value = result.name;
      document.getElementById("surname").value = result.surname;
      document.getElementById("username").value = result.username;
      document.getElementById("password").value = result.password;
      document.getElementById("role").value = result.role;
      document.getElementById("birthDate").value = result.birthDate;
      document.getElementById("email").value = result.email;
    }
  } catch (e) {
    console.log(e);
  }
}

//Función para obtener los datos del front al registrar un usuario editado
async function editUser() {
  try {
    const text = document.getElementById("resgisterResponse");
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const role = document.getElementById("role").value;
    const birthDate = document.getElementById("birthDate").value;
    const email = document.getElementById("email").value;

    const url =
      "https://localhost:44368/put/user/" + localStorage.getItem("oldUserID");

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
      body: JSON.stringify({
        name: name,
        surname: surname,
        username: username,
        password: password,
        role: role,
        birthDate: birthDate,
        email: email,
      }),
    });
    const result = await response.json();
    if (result != null) {
      text.innerHTML = "Has editado el usuario correctamente";
    } else {
      text.innerHTML =
        "No se ha podido editar el usuario, comprueba que has introducido todos los datos bien";
    }
  } catch (e) {
    console.log(e);
  }
}

async function logedIn() {
  const textLogIn = document.getElementById("userLogedIn");
  if (localStorage.username != null) {
    textLogIn.innerHTML = localStorage.surname + ", " + localStorage.name;
  }
}

window.addEventListener("load", start, false);
window.addEventListener("load", logedIn, false);
