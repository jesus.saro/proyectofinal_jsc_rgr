//Función "load"
function start() {
  setCarData();
  const buttonSend = document.getElementById("send");
  buttonSend.addEventListener("click", editCar, false);
}

//Obtener los datos del coche que va a ser modificado
async function setCarData() {
  const text = document.getElementById("result");
  try {
    const url =
      "https://localhost:44368/get/boughtCar/" +
      localStorage.getItem("oldBoughtCarID");

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
    });
    const result = await response.json();
    if (result !== null) {
      document.getElementById("brand").value = result.brand;
      document.getElementById("model").value = result.model;
      document.getElementById("yearProduced").value = result.yearProduced;
      document.getElementById("mileage").value = result.mileage;
      document.getElementById("doors").value = result.doors;
      document.getElementById("carPlate").value = result.carPlate;
      document.getElementById("colour").value = result.colour;
      document.getElementById("price").value = result.price;
    }
  } catch (e) {
    console.log(e);
  }
}

//Editar un coche que esta comprado
async function editCar() {
  const text = document.getElementById("result");
  try {
    const brand = document.getElementById("brand").value;
    const model = document.getElementById("model").value;
    const yearProduced = document.getElementById("yearProduced").value;
    const type = document.getElementById("type").value;
    const mileage = document.getElementById("mileage").value;
    const doors = document.getElementById("doors").value;
    const carPlate = document.getElementById("carPlate").value;
    const colour = document.getElementById("colour").value;
    const price = document.getElementById("price").value;

    const url =
      "https://localhost:44368/put/boughtCar/" +
      localStorage.getItem("oldBoughtCarID");

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        brand: brand,
        model: model,
        yearProduced: yearProduced,
        type: type,
        mileage: mileage,
        doors: doors,
        carPlate: carPlate,
        colour: colour,
        price: price,
        userID: localStorage.getItem("id"),
        oldCarID: localStorage.getItem("oldBoughtCarID"),
      }),
    });
    const result = await response.json();
    if (result != null) {
      text.innerHTML = "Se ha actualizado su vehículo correctamente"; //Mensaje si se ha realizado la operación
    } else {
      text.innerHTML =
        "Lo sentimos, ha ocurrido algun tipo de error al publicar su vehículo"; //Mensaje si no se ha realizado
    }
  } catch (e) {
    console.log(e);
  }
}

async function logedIn() {
  const textLogIn = document.getElementById("userLogedIn");
  const buttonLogIn = document.getElementById("logInButton");
  const userImage = document.getElementById("userImage");
  textLogIn.style.visibility = "hidden";
  userImage.style.visibility = "hidden";
  if (localStorage.username != null) {
    textLogIn.style.visibility = "visible";
    userImage.style.visibility = "visible";
    textLogIn.innerHTML = localStorage.surname + ", " + localStorage.name;
    buttonLogIn.innerHTML = "Log Out";
  } else {
    textLogIn.style.visibility = "hidden";
    buttonLogIn.innerHTML = "Log In";
    userImage.style.visibility = "hidden";
  }
}

async function redirectLogin() {
  const buttonLogIn = document.getElementById("logInButton");
  if (buttonLogIn.innerHTML === "Log In") {
    window.location.href = "login.html";
  } else {
    if (localStorage.username != null) {
      const text = document.getElementById("loginResponse");
      const textLogIn = document.getElementById("logedIn");
      localStorage.removeItem("name");
      localStorage.removeItem("surname");
      localStorage.removeItem("username");
      localStorage.removeItem("role");
      localStorage.removeItem("id");
      window.location.href = "login.html";
    }
  }
  const textLogIn = document.getElementById("logedIn");
}

window.addEventListener("load", start, false);
window.addEventListener("load", setCarData, false);
window.addEventListener("load", logedIn, false);
