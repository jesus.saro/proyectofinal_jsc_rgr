//Función "load"
function start() {
  const buttonLogin = document.getElementById("sendLogIn");
  buttonLogin.addEventListener("click", login, false);
  const buttonLogout = document.getElementById("logout");
  if (localStorage.getItem("username") === null) {
    buttonLogout.style.visibility = "hidden";
  } else {
    buttonLogout.style.visibility = "visible";
  }

  buttonLogout.addEventListener("click", logout, false);
}
let result;

//Función para obtener los datos del login
async function login(e) {
  e.preventDefault();

  try {
    const text = document.getElementById("loginResponse");
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    console.log({
      username: username,
      password: password,
    });

    const url = "https://localhost:44368/post/login";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        mode: "cors",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
    result = await response.json();

    if (result != null) {
      localStorage.setItem("name", result.name);
      localStorage.setItem("surname", result.surname);
      localStorage.setItem("username", result.username);
      localStorage.setItem("role", result.role);
      localStorage.setItem("id", result.id);
      text.innerHTML = "Te has logeado correctamente";
    } else {
      text.innerHTML =
        "No se ha podido acceder a tu cuenta, comprueba que has introducido bien el usuario o la contraseña";
    }
    logedIn();
    //poner localstorrage el rol y el id
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    text.innerHTML = result.message;
  }
}

async function logedIn() {
  const textLogIn = document.getElementById("userLogedIn");
  const buttonLogIn = document.getElementById("logInButton");
  const userImage = document.getElementById("userImage");
  textLogIn.style.visibility = "hidden";
  userImage.style.visibility = "hidden";
  if (localStorage.username != null) {
    textLogIn.style.visibility = "visible";
    userImage.style.visibility = "visible";
    textLogIn.innerHTML = localStorage.surname + ", " + localStorage.name;
    buttonLogIn.innerHTML = "Log Out";
    const buttonLogout = document.getElementById("logout");
    buttonLogout.style.visibility = "visible";
    const buttonRegister = document.getElementById("register");
    buttonRegister.style.visibility = "hidden";
  } else {
    textLogIn.style.visibility = "hidden";
    buttonLogIn.innerHTML = "Log In";
    userImage.style.visibility = "hidden";
  }
}

async function redirectLogin() {
  const buttonLogIn = document.getElementById("logInButton");
  if (buttonLogIn.innerHTML === "Log In") {
    window.location.href = "login.html";
  } else {
    if (localStorage.username != null) {
      const text = document.getElementById("loginResponse");
      const textLogIn = document.getElementById("logedIn");
      localStorage.removeItem("name");
      localStorage.removeItem("surname");
      localStorage.removeItem("username");
      localStorage.removeItem("role");
      localStorage.removeItem("id");
      window.location.href = "login.html";
    }
  }
  const textLogIn = document.getElementById("logedIn");
}

async function logout() {
  if (localStorage.username != null) {
    const text = document.getElementById("loginResponse");
    const textLogIn = document.getElementById("logedIn");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    localStorage.removeItem("username");
    localStorage.removeItem("role");
    localStorage.removeItem("id");
    text.innerHTML = "Te has des-logueado correctamente";
    textLogIn.innerHTML = "";
    const buttonLogout = document.getElementById("logout");
    buttonLogout.style.visibility = "hidden";
    const buttonRegister = document.getElementById("register");
    buttonRegister.style.visibility = "visible";
  }
}

window.addEventListener("load", start, false);
window.addEventListener("load", logedIn, false);
