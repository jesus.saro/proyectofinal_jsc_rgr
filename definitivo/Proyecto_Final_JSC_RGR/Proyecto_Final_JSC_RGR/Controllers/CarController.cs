﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Proyecto_Final_JSC_RGR.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Proyecto_Final_JSC_RGR.databaseManager;

namespace Proyecto_Final_JSC_RGR.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CarController : ApiController
    {
        // GET --> Gets all cars in the DB
        [Route("get/cars")]
        public List<Car> Get()
        {
            List <Car> lista = Methods.getCarList();
            return lista;

        }

        //GET --> Seach a car in the DB by its ID
        [Route("get/car/{id}")]
        public Car GetByID(int id)
        {
            Car res = Methods.getCarById(id);
            return res;
        }

        //GET --> Seach a boughtCar in the DB by its ID
        [Route("get/boughtCar/{id}")]
        public CarBought GetBoughtCarByID(int id)
        {
            CarBought res = Methods.getCarBoughtById(id);
            return res;
        }

        // POST --> Add a new car to the DB
        [Route("post/car")]
        [HttpPost]
        public Car Post([FromBody] CarRequest newCarRequest)
        {

            Car newCar = new Car(newCarRequest);
            int userId = Int32.Parse(newCarRequest.userID);
            User owner = Methods.getUserById(userId);
            try
            {
                Methods.createCarPost(newCar , owner);
                return newCar;
            }
            catch
            {
                return null;
            }
        }

        // POST --> Add a bought car to the DB
        [Route("post/sellBoughtCar/{carID}")]
        [HttpPost]
        public Car SellBoughtCar(int carID, [FromBody] int userID)
        {

            Car sellcar = new Car(Methods.getCarBoughtById(carID));
            User owner = Methods.getUserById(userID);
            try
            {
                Methods.deleteBoughtCar(carID);
                Methods.createCarPost(sellcar, owner);
                return sellcar;
            }
            catch
            {
                return null;
            }
        }

        // POST --> User buys a new car and its added to it's bougth cars list
        [Route("post/buyCar/{carID}")]
        [HttpPost]
        public Car BuyCar(int carID, [FromBody] int userID) //Receives the buyer's id
        {

            Car boughtCar = Methods.getCarById(carID);
            User owner = Methods.getUserById(userID);
            User seller = Methods.getUserById(boughtCar.idOwner);
            int id = owner.id;
            int quantity = boughtCar.price;

            try
            {

                Methods.deleteCar(carID);
                Methods.createCarBought(boughtCar, owner);
                Methods.sumUserFunds(seller.id , quantity);
                Methods.subUserFunds(owner.id , quantity);
                return boughtCar;
            }
            catch
            {
                return null;
            }
        }

        // PUT --> Edits an existing Car in the DB

        /*
         *Preguntar por que devuelve un id que siempre es 0 
         */

        [Route("put/car/{id}")]
        [HttpPost]
        public Car Put([FromBody] CarRequest editedCarRequest)
        {
            Car editedCar = new Car(editedCarRequest);
            if(Methods.modifyCarBought(editedCarRequest))
            {
                return editedCar;
            }
            else
            {
                return null;
            }
        }



        [Route("put/boughtCar/{id}")]
        [HttpPost]
        public CarBought PutBoughtCar([FromBody] CarRequest editedCarRequest)
        {
            CarBought editedCar = new CarBought(editedCarRequest);
            if (Methods.modifyCarBought(editedCarRequest))
            {
                return editedCar;
            }
            else
            {
                return null;
            }
        }

        // DELETE --> Deletes a bought in the DB
        [Route("delete/boughtCar/{carID}")]
        public CarBought DeleteBought(int carID, [FromBody] int userID)
        {
            CarBought deleteCar = Methods.getCarBoughtById(carID);
            if (Methods.deleteBoughtCar(carID))
            {
                return deleteCar;
            }
            else
            {
                return null;
            }            
        }

        // DELETE --> Deletes an existing car in the DB
        [Route("delete/inSaleCar/{carID}")]
        public Car Delete(int carID)
        {

            Car deleteCar = Methods.getCarById(carID);
            Methods.deleteCar(carID);

            return deleteCar;
        }

        // GET --> Gets the bought cars from a specific user in the DB
        [Route("get/boughtCars/{id}")]
        public List<CarBought> GetListBoughtCars(int id)
        {
            User user = Methods.getUserById(id);
            return Methods.getBoughtCarListSpecificUser(user);
        }


        public static Car findCarByID(string id)
        {
            Car res = Methods.getCarById(int.Parse(id));
            return res;
        }

    }
}
