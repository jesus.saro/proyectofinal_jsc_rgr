﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Proyecto_Final_JSC_RGR.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Proyecto_Final_JSC_RGR.databaseManager;

namespace Proyecto_Final_JSC_RGR.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {

        // GET --> Gets users in the DB
        [Route("get/users")]
        public List<User> GetUsers()
        {
            return Methods.getUserList();
        }


        // GET --> Gets the cars from a specific user in the DB
        [Route("get/userCars/{id}")]
        public List<Car> GetListCars(int id)
        {
            User user = Methods.getUserById(id);
            return Methods.getCarSingleUser(user);
        }


        // GET --> Seach a user in the DB by its ID
        [Route("get/user/{id}")]
        public User GetUser(int id)
        {
            return Methods.getUserById(id);
        }


        // POST --> Add a new user to the DB
        [Route("post/user")]
        [HttpPost]
        public User PostUser([FromBody] User newUser)
        {
            if (Methods.createUser(newUser))
            {
                return newUser;
            }
            else
            {
                return null;
            }
        }


        // POST --> Check if the introduced user and password are correct, returns the user loged in
        [Route("post/login")]
        [HttpPost]
        public User Login([FromBody] Login newLogin)
        {
            return Methods.logVerification(newLogin.username , newLogin.password);
        }

        // PUT --> Edits an existing User in the DB
        [Route("put/user/{id}")]
        [HttpPost]
        public User EditUser(int id, [FromBody] User editedUser)
        {
            if(Methods.modifyUser(editedUser , id))
            {
                return editedUser;
            }
            else
            {
                return null;
            }
        }

        // DELETE --> Deletes an existing user in the DB
        [Route("delete/user/{id}")]
        public User Delete(int id)
        {
            User deleteUser = Methods.getUserById(id);
            Methods.deleteUser(id);

            return deleteUser;
        }

        // GET --> Gets the funds of an specified user in the DB
        [Route("get/userFunds/{id}")]
        public int GetUserFunds(int id)
        {
            return Methods.getUserById(id).funds;
        }


        // GET --> Adds funds to an specified user
        [Route("post/addFunds/{id}")]
        [HttpPost]
        public int AddOrRetriveUserFunds(int id, [FromBody] int quantity)
        {
            return Methods.sumUserFunds(id , quantity);
        }
    }
}
