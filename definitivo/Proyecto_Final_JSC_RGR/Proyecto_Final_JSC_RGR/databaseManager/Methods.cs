﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_Final_JSC_RGR.Models;

namespace Proyecto_Final_JSC_RGR.databaseManager
{
    public class Methods
    {
        //MANEJO DE COCHES


        /*
         * SIRVE PARA INSTANCIAR COCHES EN LA BASE DE DATOS
         * 
         * Pasas por parametro el usuario que va a vender el coche y el
         * propio coche 
         * 
         * El ejemplo de la utilizacion de este metodo es : 
         * 
         * User user;
         * createCarPost(new car (...) , user);
         */
        public static Boolean createCarPost(Car car, User owner)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    car.idOwner = owner.id;
                    db.Car.Add(car);


                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static Boolean modifyCar(CarRequest car)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    var query = "UPDATE Car " +
                        "SET brand = '" + car.brand + "' , " +
                        "model = '" + car.model + "' , " +
                        "yearProduced = " + car.yearProduced + " , " +
                        "Car.type = '" + car.type + "' , " +
                        "mileage = " + car.mileage + " , " +
                        "doors = " + car.doors + " , " +
                        "carPlate = '" + car.carPlate + "' , " +
                        "colour = '" + car.colour + "' , " +
                        "price = " + car.price + "" +
                        "WHERE id = " + car.oldCarID + ";";
                    db.Database.ExecuteSqlCommand(query);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        //Metodo que devuelve todos los coches de la base de datos

        public static List<Car> getCarList()
        {
            List<Car> result = new List<Car>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result = db.Car.ToList<Car>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }


        /*
         * Devuelve los coches que el usuario pasado por parametro 
         * ha guardado
         * 
         * En caso de no encontrar el usuario o que no tenga coches
         * devolvera una lista vacia
         */

        public static List<Car> getCarSingleUser(User owner)
        {
            List<Car> result = new List<Car>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result = db.Car
                        .Where(s => s.idOwner == owner.id)
                        .ToList<Car>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }

        /*
         *Buscar coche en la db introduciendo su id
         */

        public static Car getCarById(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.Car
                        .Single(s => s.id == id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }



        /*
         * Elimina el coche de la base de datos
         * segun su id
         */
        public static Boolean deleteCar(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    Car carToDelete = db.Car
                        .Where(s => s.id == id)
                        .First<Car>();

                    db.Car.Remove(carToDelete);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }


        //MANEJO DE USUARIOS


        /*
         *Buscar usuarios en la db introduciendo su id
         */
        public static User getUserById(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.User
                        .Single(s => s.id == id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /*
         * METODO PARA INTRODUCIR UN USUARIO A LA BASE DE DATOS
         */
        public static Boolean createUser(User user)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    db.User.Add(user);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }


        //Metodo que devuelve todos los usuarios de la base de datos

        public static List<User> getUserList()
        {
            List<User> result = new List<User>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result = db.User.ToList<User>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }

        //Editar usuario

        public static Boolean modifyUser(User user, int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    var query = "UPDATE \"User\" " +
                        "SET name = '" + user.name + "' , " +
                        "surname = '" + user.surname + "' , " +
                        "password = " + user.password + " , " +
                        "username = '" + user.username + "' , " +
                        "role = '" + user.role + "' , " +
                        "birthDate = '" + user.birthDate + "' , " +
                        "email = '" + user.email + "' " +
                        "WHERE id = " + id + ";";

                    db.Database.ExecuteSqlCommand(query);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }


        /*
         * Elimina el usuario de la base de datos
         * segun su id
         */

        public static Boolean deleteUser(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    User userToDelete = db.User
                        .Where(s => s.id == id)
                        .First<User>();

                    var query = "DELETE \"User\" WHERE id = " + id + ";";
                    db.Database.ExecuteSqlCommand(query);
                    db.User.Remove(userToDelete);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static Boolean insertUserConcretId(int id, User user)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    user.id = id;
                    db.User.Add(user);

                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        //Busca usuario con contraseña y usuario indicados

        public static User logVerification(string username, string password)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.User
                        .Where(s => s.password == password && s.username == username)
                        .First<User>();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public static int sumUserFunds(int id, int quantity)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    User user = getUserById(id);
                    var query = "UPDATE \"User\" SET funds = funds + " + quantity + " WHERE id = " + id + ";";
                    db.Database.ExecuteSqlCommand(query);
                    db.SaveChanges();
                    return user.funds + quantity;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return -1;
            }
        }

        public static int subUserFunds(int id, int quantity)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    User user = getUserById(id);
                    var query = "UPDATE \"User\" SET funds = funds - " + quantity + " WHERE id = " + id + ";";
                    db.Database.ExecuteSqlCommand(query);
                    db.SaveChanges();
                    return user.funds + quantity;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return -1;
            }
        }

        //MANEJO COCHE COMPRADO

        //Añadir coche comprado
        public static Boolean createCarBought(Car car, User owner)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    CarBought carToIntroduce = new CarBought(car);
                    carToIntroduce.idOwner = owner.id;
                    db.CarBought.Add(carToIntroduce);

                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static User getOwnerBoughtCar(CarBought car)
        {
            try
            {
                int idOwner = car.idOwner;
                return getUserById(idOwner);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }


        //Lista de coches comprados de una persona

        public static List<CarBought> getBoughtCarListSpecificUser(User owner)
        {
            List<CarBought> result = new List<CarBought>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result = db.CarBought
                        .Where(s => s.idOwner == owner.id)
                        .ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }
        //Modificar coche comprado
        public static Boolean modifyCarBought(CarRequest car)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    var query = "UPDATE CarBought " +
                        "SET brand = '" + car.brand + "' , " +
                        "model = '" + car.model + "' , " +
                        "yearProduced = " + car.yearProduced + " , " +
                        "CarBought.type = '" + car.type + "' , " +
                        "mileage = " + car.mileage + " , " +
                        "doors = " + car.doors + " , " +
                        "carPlate = '" + car.carPlate + "' , " +
                        "colour = '" + car.colour + "' , " +
                        "price = " + car.price + " " +
                        "WHERE id = " + car.oldCarID + ";";
                    db.Database.ExecuteSqlCommand(query);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }


        //Eliminar coche comprado

        public static Boolean deleteBoughtCar(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    CarBought carToDelete = db.CarBought
                        .Where(s => s.id == id)
                        .First<CarBought>();

                    db.CarBought.Remove(carToDelete);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static CarBought getCarBoughtById(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.CarBought
                        .Single(s => s.id == id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        //Extras


        public static int count()
        {
            using (databaseEntities db = new databaseEntities())
            {
                return db.Car.Count<Car>();
            }
        }
    }
}