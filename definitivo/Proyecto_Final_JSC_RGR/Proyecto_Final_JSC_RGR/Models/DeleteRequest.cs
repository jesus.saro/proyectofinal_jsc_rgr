﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final_JSC_RGR.Models
{
    public class DeleteRequest
    {

        public string carId { get; set; }
        public string userID { get; set; }


        public DeleteRequest(string carId, string userID)
        {

            this.carId = carId;
            this.userID = userID;
        }

        //public Car() { }
        
    }
}