﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final_JSC_RGR.Models
{
    public class Login
    {
        public string username { get; set; }

        public string password { get; set; }

        public Login(string username, string password) {
            this.username = username;
            this.password = password;
        }
    }
}