﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final_JSC_RGR.Models
{
    public class CarRequest
    {
        public int id { get; set; }
        public string brand { get; set; }

        public string model { get; set; }

        public int yearProduced { get; set; }

        public string type { get; set; }

        public int mileage { get; set; }

        public int doors { get; set; }

        public string carPlate { get; set; }

        public string colour { get; set; }

        public int price { get; set; }

        public string userID { get; set; }
        public int oldCarID{ get; set; }


        public CarRequest(String brand, string model, int yearProduced, string type, int mileage, int doors, string carPlate, string colour, int price , int oldCarID)
        {

            this.brand = brand;
            this.model = model;
            this.yearProduced = yearProduced;
            this.type = type;
            this.mileage = mileage;
            this.doors = doors;
            this.carPlate = carPlate;
            this.colour = colour;
            this.price = price;
            this.userID = userID;
            this.oldCarID = oldCarID;
        }
        //public Car() { }
    }
}