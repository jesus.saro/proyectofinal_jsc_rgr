﻿using System.Web;
using System.Web.Mvc;

namespace Proyecto_Final_JSC_RGR
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
