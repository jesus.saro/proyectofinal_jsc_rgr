using System;
using Xunit;
using databaseManajer;

namespace TestCarProyect
{
    public class UnitTest1
    {
        [Fact]
        public void createCar()
        {

            using (databaseEntities db = new databaseEntities())
            {
                int numElems = databaseManajer.Program.count();
                databaseManajer.Program.create(2, "opel", "corsa", "coupe", 40000, 4, "3232TLQ", "red");
                Assert.Equal(databaseManajer.Program.count(), numElems + 1);
            }
            
        }

        [Fact]
        public void DeleteCar()
        {
            int numElems = databaseManajer.Program.count();
            databaseManajer.Program.delete(2);
            Assert.Equal(databaseManajer.Program.count(), numElems -1);
        }
    }
}
