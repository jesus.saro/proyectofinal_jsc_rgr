﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Db = (localdb)\Servidor

namespace databaseManajer
{
    public class Program
    {
        static void Main(string[] args)
        {

            Console.ReadLine();

        }
        

        //MANEJO DE COCHES


        /*
         * SIRVE PARA INSTANCIAR COCHES EN LA BASE DE DATOS
         * 
         * Pasas por parametro el usuario que va a vender el coche y el
         * propio coche 
         * 
         * El ejemplo de la utilizacion de este metodo es : 
         * 
         * User user;
         * createCarPost(new car (...) , user);
         */
        public static Boolean createCarPost(Car car , User owner)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    car.idOwner = owner.id;
                    db.Car.Add(car);

                    db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static Boolean insertCarConcretId(int id , Car car)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    car.id = id;
                    db.Car.Add(car);

                    db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        //Metodo que devuelve todos los coches de la base de datos

        public static List<Car> getCarList()
        {
            List<Car> result = new List<Car>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result= db.Car.ToList<Car>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }


        /*
         * Devuelve los coches que el usuario pasado por parametro 
         * ha guardado
         * 
         * En caso de no encontrar el usuario o que no tenga coches
         * devolvera una lista vacia
         */

        public static List<Car> getCarSingleUser(User owner)
        {
            List<Car> result = new List<Car>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result = db.Car
                        .Where(s => s.idOwner == owner.id)
                        .ToList<Car>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }

        /*
         *Buscar coche en la db introduciendo su id
         */

        public static Car getCarById(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.Car
                        .Single(s => s.id == id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }


        /*
         * Elimina el coche de la base de datos
         * segun su id
         */
        public static Boolean deleteCar (int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {

                    Car carToDelete = db.Car
                        .Where(s => s.id == id)
                        .First<Car>();

                    db.Car.Remove(carToDelete);
                    db.SaveChangesAsync();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }


        //MANEJO DE USUARIOS


        /*
         *Buscar usuarios en la db introduciendo su id
         */
        public static User getUserById(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.User
                        .Single(s => s.id == id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /*
         * METODO PARA INTRODUCIR UN USUARIO A LA BASE DE DATOS
         */
        public static Boolean createUser(User user)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    db.User.Add(user);
                    db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }


        //Metodo que devuelve todos los usuarios de la base de datos

        public static List<User> getUserList()
        {
            List<User> result = new List<User>();
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    result = db.User.ToList<User>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return result;
        }


        /*
         * Elimina el usuario de la base de datos
         * segun su id
         */

        public static Boolean deleteUser(int id)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    User userToDelete = db.User
                        .Where(s => s.id == id)
                        .First<User>();

                    db.User.Remove(userToDelete);
                    db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static Boolean insertUserConcretId(int id, User user)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    user.id = id;
                    db.User.Add(user);

                    db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        //Busca usuario con contraseña y usuario indicados

        public static User logVerification(string username , string password)
        {
            try
            {
                using (databaseEntities db = new databaseEntities())
                {
                    return db.User
                        .Where(s => s.password == password && s.username == username)
                        .First<User>();
                }

            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        //Extras


        public static int count()
        {
            using (databaseEntities db = new databaseEntities())
            {
                return db.Car.Count<Car>();
            }
        }

    }
}
