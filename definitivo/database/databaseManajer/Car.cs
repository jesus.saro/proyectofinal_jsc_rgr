//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace databaseManajer
{
    using Proyecto_Final_JSC_RGR.Models;
    using System;
    using System.Collections.Generic;
    
    public partial class Car
    {
        public int id { get; set; }
        public int idOwner { get; set; }
        public string brand { get; set; }
        public string model { get; set; }
        public int yearProduced { get; set; }
        public string type { get; set; }
        public int mileage { get; set; }
        public int doors { get; set; }
        public string carPlate { get; set; }
        public string colour { get; set; }
        public int price { get; set; }

        public Car(String brand, string model, int yearProduced, string type, int mileage, int doors, string carPlate, string colour, int price)
        {

            this.brand = brand;
            this.model = model;
            this.yearProduced = yearProduced;
            this.type = type;
            this.mileage = mileage;
            this.doors = doors;
            this.carPlate = carPlate;
            this.colour = colour;
            this.price = price;
        }
        public Car()
        {

        }

        public Car(CarRequest request)
        {
            this.brand = request.brand;
            this.model = request.model;
            this.yearProduced = request.yearProduced;
            this.type = request.type;
            this.mileage = request.mileage;
            this.doors = request.doors;
            this.carPlate = request.carPlate;
            this.colour = request.colour;
            this.price = request.price;
        }
    }
}
